<?php
/**
 * 发表评论
 *
 * @copyright (c) Emlog All Rights Reserved
 * Translate By : Bashqut 
 * http://Bashqut.com
 */

class Comment_Controller {
    function addComment($params) {
        $name = isset($_POST['comname']) ? addslashes(trim($_POST['comname'])) : '';
        $content = isset($_POST['comment']) ? addslashes(trim($_POST['comment'])) : '';
        $mail = isset($_POST['commail']) ? addslashes(trim($_POST['commail'])) : '';
        $url = isset($_POST['comurl']) ? addslashes(trim($_POST['comurl'])) : '';
        $imgcode = isset($_POST['imgcode']) ? addslashes(trim(strtoupper($_POST['imgcode']))) : '';
        $blogId = isset($_POST['gid']) ? intval($_POST['gid']) : -1;
        $pid = isset($_POST['pid']) ? intval($_POST['pid']) : 0;

        if (ISLOGIN === true) {
            $CACHE = Cache::getInstance();
            $user_cache = $CACHE->readCache('user');
            $name = addslashes($user_cache[UID]['name_orig']);
            $mail = addslashes($user_cache[UID]['mail']);
            $url = addslashes(BLOG_URL);
        }

        if ($url && strncasecmp($url,'http',4)) {
            $url = 'http://'.$url;
        }

        doAction('comment_post');

        $Comment_Model = new Comment_Model();
        $Comment_Model->setCommentCookie($name,$mail,$url);
        if($Comment_Model->isLogCanComment($blogId) === false) {
            emMsg('مەغلۇپ بولدى: بۇ يازما ئىنكاس قۇبۇل قىلمايدۇ');
        } elseif ($Comment_Model->isCommentExist($blogId, $name, $content) === true) {
            emMsg('مەغلۇپ بولدى: ئوخشاش ئىنكاس بار');
        } elseif (ROLE == ROLE_VISITOR && $Comment_Model->isCommentTooFast() === true) {
            emMsg('سىزنىڭ ئىنكاس تاپشۇرۇشىڭىز بەك تېز ، سەل تۇرۇپ قايتا سىناڭ');
        } elseif (empty($name)) {
            emMsg('ئىسىمنى تولدۇرۇڭ');
        } elseif (strlen($name) > 20) {
            emMsg('ئىسىم ئۆلچەملىك ئەمەس');
        } elseif ($mail != '' && !checkMail($mail)) {
            emMsg('ئېلخەت ئۆلچەملىك ئەمەس');
        } elseif (ISLOGIN == false && $Comment_Model->isNameAndMailValid($name, $mail) === false) {
            emMsg('ئىنكاس مەغلۇپ بولدى: باشقۇرغۇچىنىڭكىگە ئوخشاش ئىسىم ياكى ، ئېلخەت ئىشلىتىلگەن');
        } elseif (!empty($url) && preg_match("/^(http|https)\:\/\/[^<>'\"]*$/", $url) == false) {
            emMsg('تور بەت ئادرىسى ئۆلچەملىك ئەمەس','javascript:history.back(-1);');
        } elseif (empty($content)) {
            emMsg('ئىنكاس مەزمۇنىنى يېزىڭ');
        } elseif (strlen($content) > 8000) {
            emMsg('مەزمۇن ئۆلچەمگە چۇشمىگەن');
        } elseif (ROLE == ROLE_VISITOR && Option::get('comment_needchinese') == 'y' && !preg_match('/[\x{4e00}-\x{9fa5}]/iu', $content)) {
            emMsg('ئىنكاس مەزمۇنىدا خەنزۇچە خەت  بولىشى كېرەك');
        } elseif (ISLOGIN == false && Option::get('comment_code') == 'y' && session_start() && (empty($imgcode) || $imgcode !== $_SESSION['code'])) {
            emMsg('تەستىق كودى خاتالىقى');
        } else {
            $_SESSION['code'] = null;
            $Comment_Model->addComment($name, $content, $mail, $url, $imgcode, $blogId, $pid);
        }
    }
}
