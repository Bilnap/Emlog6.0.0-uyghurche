<?php
/**
 * 数据库操作路由
 *
 * @copyright (c) Emlog All Rights Reserved
 * Translate By : Bashqut 
 * http://Bashqut.com
 */

class Database {

    public static function getInstance() {
        if (class_exists('mysqli', FALSE)) {
            return MySqlii::getInstance();
        }
        else if (class_exists('mysql', FALSE)) {
            return MySql::getInstance();
        }
        else {
            emMsg('مۇلازىمىتېر PHP مۇھىتى سانداننى قوللىمىدى');
        }
    }

}
