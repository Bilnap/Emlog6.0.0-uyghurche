<?php
/**
 * 视图控制
 * @copyright (c) Emlog All Rights Reserved
 * Translate By : Bashqut 
 * http://Bashqut.com
 */

class View {
	public static function getView($template, $ext = '.php') {
		if (!is_dir(TEMPLATE_PATH)) {
			emMsg('ھازىرقى ئۇسلۇپ ئۆچۈرۈلگەن ياكى بۇزۇلغان ، ئارقا بەتتىن باشقا ئۇسلۇپ تاللاپ ئىشلىتىڭ', BLOG_URL . 'admin/template.php');
		}
		return TEMPLATE_PATH . $template . $ext;
	}

	public static function output() {
		$content = ob_get_clean();
        ob_start();
		echo $content;
		ob_end_flush();
		exit;
	}
	
}
