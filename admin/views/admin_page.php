<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="containertitle"><b>بەت يۈزى باشقۇرۇش</b>
<?php if(isset($_GET['active_del'])):?><span class="alert alert-success">بەت يۈزى ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_hide_n'])):?><span class="alert alert-success">بەت يۈزى يوللاندى</span><?php endif;?>
<?php if(isset($_GET['active_hide_y'])):?><span class="alert alert-success">بەت يۈزى چەكلەندى</span><?php endif;?>
<?php if(isset($_GET['active_pubpage'])):?><span class="alert alert-success">بەت يۈزى ساقلاندى</span><?php endif;?>
</div>
<div class=line></div>
<form action="page.php?action=operate_page" method="post" name="form_page" id="form_page">
  <table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
      <tr>
        <th width="461" colspan="2"><b>تېما</b></th>
        <th width="140"><b>ئۇسلۇپ</b></th>
        <th width="50" class="tdcenter"><b>ئىنكاس</b></th>
        <th width="140"><b>ۋاقىت</b></th>
      </tr>
    </thead>
    <tbody>
    <?php
    if($pages):
    foreach($pages as $key => $value):
    if (empty($navibar[$value['gid']]['url']))
    {
        $navibar[$value['gid']]['url'] = Url::log($value['gid']);
    }
    $isHide = $value['hide'] == 'y' ? 
    '<font color="red"> - 草稿</font>' : 
    '<a href="'.$navibar[$value['gid']]['url'].'" target="_blank" title="بەت يۈزى كۆرۈش"><img src="./views/images/vlog.gif" align="absbottom" border="0" /></a>';
    ?>
     <tr>
        <td width="21"><input type="checkbox" name="page[]" value="<?php echo $value['gid']; ?>" class="ids" /></td>
        <td width="440">
        <a href="page.php?action=mod&id=<?php echo $value['gid']?>"><?php echo $value['title']; ?></a> 
        <?php echo $isHide; ?>    
        <?php if($value['attnum'] > 0): ?><img src="./views/images/att.gif" align="top" title="قىستۇرما：<?php echo $value['attnum']; ?>" /><?php endif; ?>
        </td>
        <td><?php echo $value['template']; ?></td>
        <td class="tdcenter"><a href="comment.php?gid=<?php echo $value['gid']; ?>"><?php echo $value['comnum']; ?></a></td>
        <td class="small"><?php echo $value['date']; ?></td>
     </tr>
    <?php endforeach;else:?>
      <tr><td class="tdcenter" colspan="5">تېخى بەت يۈزى يوق</td></tr>
    <?php endif;?>
    </tbody>
  </table>
  <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
  <input name="operate" id="operate" value="" type="hidden" />
</form>
<div class="list_footer">
<a href="javascript:void(0);" id="select_all">ھەممىنى تاللاش</a> تاللىغاننى：
<a href="javascript:pageact('del');" class="care">ئۆچۈرۈش</a> | 
<a href="javascript:pageact('hide');">ئورگىنالغا ساقلاش</a> | 
<a href="javascript:pageact('pub');">يوللاش</a>
</div>
<div style="margin:20px 0px 0px 0px;"><a href="page.php?action=new" class="btn btn-success">بەت يۈزى قۇرۇش+</a></div>
<div class="page"><?php echo $pageurl; ?> (بار<?php echo $pageNum; ?>دانە بەت يۈزى)</div>
<script>
$(document).ready(function(){
    $("#adm_comment_list tbody tr:odd").addClass("tralt_b");
    $("#adm_comment_list tbody tr")
        .mouseover(function(){$(this).addClass("trover")})
        .mouseout(function(){$(this).removeClass("trover")});
    selectAllToggle();
});
setTimeout(hideActived,2600);
function pageact(act){
    if (getChecked('ids') == false) {
        alert('بەت يۈزى تاللاڭ');
        return;}
    if(act == 'del' && !confirm('راستىنلا ئۆچۈرەمسىز؟？')){return;}
    $("#operate").val(act);
    $("#form_page").submit();
}
$("#menu_page").addClass('active');
</script>
