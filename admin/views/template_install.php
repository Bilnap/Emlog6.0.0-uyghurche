﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="panel-heading">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="./template.php">ئۇسلۇب باشقۇرۇش</a></li>
        <li role="presentation" class="active"><a href="template.php?action=install">ئۇسلۇب قاچىلاش</a></li>
        <?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">پەقەت zip فورماتىدىكى ھۆججەتنىلا قوللايدۇ</span><?php endif;?>
        <?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">يوللاش مەغلۇب  بولدى ،ئۇسلۇب ھۆججىتى ئىچىدكى(content/templates)  يېزىش ھوقۇقى بىرىلمىگەن</span><?php endif;?>
        <?php if(isset($_GET['error_c'])):?><span class="alert alert-danger">بوشلۇق zip ھۆججەت قولىمايدۇ ، ئۆزىڭىز قول سېلىپ قاچىلاڭ</span><?php endif;?>
        <?php if(isset($_GET['error_d'])):?><span class="alert alert-danger">zip ھالەتتىكى ئۇسۇب ھۆججىتىنى تاللاڭ</span><?php endif;?>
        <?php if(isset($_GET['error_e'])):?><span class="alert alert-danger">قاچىلاش مەغلۇب بولدى ،ئۇسلۇب ماس كەلمىدى</span><?php endif;?>
    </ul>
</div>
<?php if(isset($_GET['error_c'])): ?>
<div style="margin:20px 20px;">
<div class="alert alert-danger">
قولدا ئۇسلۇب قاچىلاش： <br />
1- ئۇسلۇب ھۆججىتىنى يىشىپ content/templates ھۆججىتى ئىچىگە يوللاڭ<br />
2- ئارقا بەتكە كىرىپ ئۇسلۇب ئالماشتۇرڭ<br />
</div>
</div>
<?php endif; ?>
<form action="./template.php?action=upload_zip" method="post" enctype="multipart/form-data" >
<div style="margin:50px 0px 50px 20px;">
    <p>zip فورماتىدىكى ئۇسلۇب بولىقىنى يوللاڭ</p>
    <p>
    <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
    <input name="tplzip" type="file" />
    </p>
    <p>
    <input type="submit" value="قاچىلاش" class="btn btn-primary" />
    </p>
</div>
</form>
<div style="margin:10px 20px;">تېخىمۇ كۆپ ئۇسلۇب：<a href="store.php">ئەپ بازىرى&raquo;</a></div>
<script>
setTimeout(hideActived,2600);
$("#menu_category_view").addClass('active');
$("#menu_view").addClass('in');
$("#menu_tpl").addClass('active');
</script>
