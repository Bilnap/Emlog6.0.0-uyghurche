﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="containertitle"><b>ئەزا باشقۇرۇش</b>
<?php if(isset($_GET['active_del'])):?><span class="alert alert-success">ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_update'])):?><span class="alert alert-success">ئارخىپ ئۆزگەرتىلدى</span><?php endif;?>
<?php if(isset($_GET['active_add'])):?><span class="alert alert-success">ئەزا قوشۇلدى</span><?php endif;?>
<?php if(isset($_GET['error_login'])):?><span class="alert alert-danger">ئەزا ئىسمى بوش قالمىسۇن</span><?php endif;?>
<?php if(isset($_GET['error_exist'])):?><span class="alert alert-danger">بۇ ئەزا ئىسمى مەۋجۇت</span><?php endif;?>
<?php if(isset($_GET['error_pwd_len'])):?><span class="alert alert-danger">پارول 6 خانىدىن يۇقىرى بولسۇن</span><?php endif;?>
<?php if(isset($_GET['error_pwd2'])):?><span class="alert alert-danger">ئىككى قېتىملىق پارول بىردەك ئەمەس</span><?php endif;?>
<?php if(isset($_GET['error_del_a'])):?><span class="alert alert-danger">قۇرغۇچىنى ئۆچۈرگىلى بولمايدۇ</span><?php endif;?>
<?php if(isset($_GET['error_del_b'])):?><span class="alert alert-danger">قۇرۇغۇچى ئۇچۇرىنى ئۆزگەرتكىلى بولمايدۇ</span><?php endif;?>
</div>
<div class=line></div>
<form action="comment.php?action=admin_all_coms" method="post" name="form" id="form">
    <table class="table table-striped table-bordered table-hover dataTable no-footer" id="adm_comment_list">
  	<thead>
      <tr>
        <th width="60"></th>
        <th width="220"><b>ئەزا</b></th>
        <th width="250"><b>چۈشەندۈرىلىش</b></th>
        <th width="240"><b>ئېلخەت</b></th>
		<th width="30" class="tdcenter"><b>يازما</b></th>
      </tr>
    </thead>
    <tbody>
	<?php
	if($users):
	foreach($users as $key => $val):
		$avatar = empty($user_cache[$val['uid']]['avatar']) ? './views/images/avatar.jpg' : '../' . $user_cache[$val['uid']]['avatar'];
	?>
     <tr>
        <td style="padding:3px; text-align:center;"><img src="<?php echo $avatar; ?>" height="40" width="40" /></td>
		<td>
		<?php echo empty($val['name']) ? $val['login'] : $val['name']; ?><br />
		<?php echo $val['role'] == ROLE_ADMIN ? $val['uid'] == 1 ? 'قۇرغۇچى':'باشقۇرغۇچى' : 'ئاپتور'; ?>
        <?php if ($val['role'] == ROLE_WRITER && $val['ischeck'] == 'y') echo '(يازمىسى تەستىقلىنىدۇ)';?>
		<span style="display:none; margin-left:8px;">
		<?php 
        if (UID != $val['uid']): ?>
		<a href="user.php?action=edit&uid=<?php echo $val['uid']?>">تەھرىرلەش</a> 
		<a href="javascript: em_confirm(<?php echo $val['uid']; ?>, 'user', '<?php echo LoginAuth::genToken(); ?>');" class="care">ئۆچۈرۈش</a>
		<?php else:?>
		<a href="blogger.php">تەھرىرلەش</a>
		<?php endif;?>
		</span>
		</td>
		<td><?php echo $val['description']; ?></td>
		<td><?php echo $val['email']; ?></td>
		<td class="tdcenter"><a href="./admin_log.php?uid=<?php echo $val['uid'];?>"><?php echo $sta_cache[$val['uid']]['lognum']; ?></a></td>
     </tr>
	<?php endforeach;else:?>
	  <tr><td class="tdcenter" colspan="6">تېخى ئاپتور قوشۇلمايدۇ</td></tr>
	<?php endif;?>
	</tbody>
  </table>
</form>
<div class="page"><?php echo $pageurl; ?> (بار<?php echo $usernum; ?>نەپەر ئەزا)</div> 
<form action="user.php?action=new" method="post" class="form-inline">
<div style="margin:10px 0px 30px 0px;"><a href="javascript:displayToggle('user_new', 2);" class="btn btn-success">ئەزا قوشۇش+</a></div>
<div id="user_new" class="form-group">
    <li>
	<select name="role" id="role" class="form-control">
		<option value="writer">ئاپتور（ئەھەر يازغۇچى）</option>
		<option value="admin">باشقۇرغۇچى</option>
	</select>
	</li>
	<li><input name="login" type="text" id="login" value="" style="width:180px;" class="form-control" /> ئەزا نامى</li>
	<li><input name="password" type="password" id="password" value="" style="width:180px;" class="form-control" /> پارول (6 خانىدىن چوڭ بولسۇن)</li>
	<li><input name="password2" type="password" id="password2" value="" style="width:180px;" class="form-control" /> قايتىلانما پارول</li>
	<li id="ischeck">
	<select name="ischeck" class="form-control">
        <option value="n">ئەسىرى تەستىقلانمىسۇن</option>
		<option value="y">ئەسىرى تەستىقلانسۇن</option>
	</select>
	</li>
    <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
	<li><input type="submit" name="" value="ئەزا قوشۇش" class="btn btn-primary" /></li>
</div>
</form>
<script>
$("#user_new").css('display', $.cookie('em_user_new') ? $.cookie('em_user_new') : 'none');
$(document).ready(function(){
	$("#adm_comment_list tbody tr:odd").addClass("tralt_b");
	$("#adm_comment_list tbody tr")
		.mouseover(function(){$(this).addClass("trover");$(this).find("span").show();})
		.mouseout(function(){$(this).removeClass("trover");$(this).find("span").hide();})
    $("#role").change(function(){$("#ischeck").toggle()})
});
setTimeout(hideActived,2600);
$("#menu_sys").addClass('in');
$("#menu_user").addClass('active');
</script>
