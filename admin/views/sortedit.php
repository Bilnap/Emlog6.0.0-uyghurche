﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">تۈر ئىسمى بوش قالمىسۇن</span><?php endif;?>
<?php if(isset($_GET['error_c'])):?><span class="alert alert-danger">باشقا ئىسىم فورماتىدا خاتالىق بار</span><?php endif;?>
<?php if(isset($_GET['error_d'])):?><span class="alert alert-danger">باشقا ئىسىم تەكرارلانمىسۇن</span><?php endif;?>
<?php if(isset($_GET['error_e'])):?><span class="alert alert-danger">باشقا ئىسىمگە ئەسلى سېستىمىدا بار بولغان خەتكۈچلەر قوشۇلمىسۇن</span><?php endif;?>
<div class=containertitle><b>تۈر تەھرىرلەش</b></div>
<form action="sort.php?action=update" method="post" class="form-inline">
<div class="form-group">
    <li>
        <input style="width:200px;" value="<?php echo $sortname; ?>" name="sortname" id="sortname" class="form-control" />
        <label>ئىسىم</label>
    </li>
    <li>
        <input style="width:200px;" value="<?php echo $alias; ?>" name="alias" id="alias" class="form-control" />
        <label>باشقا ئىسىم</label>
    </li>
    <?php if (empty($sorts[$sid]['children'])): ?>
    <li>
        <select name="pid" id="pid" class="form-control" style="width:200px;">
            <option value="0"<?php if($pid == 0):?> selected="selected"<?php endif; ?>>يوق</option>
            <?php
                foreach($sorts as $key=>$value):
                    if ($key == $sid || $value['pid'] != 0) continue;
            ?>
            <option value="<?php echo $key; ?>"<?php if($pid == $key):?> selected="selected"<?php endif; ?>><?php echo $value['sortname']; ?></option>
            <?php endforeach; ?>
        </select>
        <label>ئاتا تۈر</label>
    </li>
    <?php endif; ?>
    <li><input maxlength="200" style="width:200px;" class="form-control" name="template" id="template" value="<?php echo $template; ?>" /> ئۇسلۇب (تۈر ئۇسلۇبنى ئۆزىڭىز خالىغانچە تەڭشىمەكچى بولسىڭىز ،ئۇسلۇب ھۆججەت ئىسمىنى .php بەلگىلەڭ(كۆڭۈلدىكىسى log_list.php)</li>
    <li>
        <textarea name="description" type="text" style="width:360px;height:80px;overflow:auto;" class="form-control" placeholder="چۈشەندۈرىلىشى"><?php echo $description; ?></textarea>
    </li>
    <li>
    <input type="hidden" value="<?php echo $sid; ?>" name="sid" />
    <input type="submit" value="ساقلاش" class="btn btn-primary" id="save"  />
    <input type="button" value="قايتىش" class="btn btn-default" onclick="javascript: window.history.back();" />
    <span id="alias_msg_hook"></span>
    </li>
</div>
</form>
<script>
$("#menu_sort").addClass('active');
$("#alias").keyup(function(){checksortalias();});
function issortalias(a){
    var reg1=/^[\w-]*$/;
    var reg2=/^[\d]+$/;
    if(!reg1.test(a)) {
        return 1;
    }else if(reg2.test(a)){
        return 2;
    }else if(a=='post' || a=='record' || a=='sort' || a=='tag' || a=='author' || a=='page'){
        return 3;
    } else {
        return 0;
    }
}
function checksortalias(){
    var a = $.trim($("#alias").val());
    if (1 == issortalias(a)){
        $("#save").attr("disabled", "disabled");
        $("#alias_msg_hook").html('<span id="input_error">باشقا ئىسىم خاتا ، چوقۇم ھەرىپ،سان،ئاستى سىزىق قاتارلىقلار ئىشلىتىلسۇن</span>');
    }else if (2 == issortalias(a)){
        $("#save").attr("disabled", "disabled");
        $("#alias_msg_hook").html('<span id="input_error">باشقا ئىسىم ساپ سان بولمىسۇن</span>');
    }else if (3 == issortalias(a)){
        $("#save").attr("disabled", "disabled");
        $("#alias_msg_hook").html('<span id="input_error">باشقا ئىسىم ۋە سېستىما ئۇلىنىشى توقۇنۇشىپ قالدى</span>');
    }else {
        $("#alias_msg_hook").html('');
        $("#msg").html('');
        $("#save").attr("disabled", false);
    }
}
</script>