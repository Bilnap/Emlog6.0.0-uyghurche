<?php if(!defined('EMLOG_ROOT')) {exit('error!');} ?>
<div class="containertitle"><b>ساندان زاپاسلاش</b>
<?php if(isset($_GET['active_del'])):?><span class="alert alert-success">زاپاس ھۆججەت ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_backup'])):?><span class="alert alert-success">ساندان زاپاسلاندى</span><?php endif;?>
<?php if(isset($_GET['active_import'])):?><span class="alert alert-success">زاپاس كىرگۈزۈلدى</span><?php endif;?>
<?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">ئۆچۈرمەكچى بولغان زاپاس ھۆججەتنى تاللاڭ</span><?php endif;?>
<?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">ساندان ھۆججەت ئىسمى خاتا بولۇپ قالدى (ئىنگىلىسچە ھەرب، سان ۋە سىزىقچە بولسۇن)</span><?php endif;?>
<?php if(isset($_GET['error_c'])):?><span class="alert alert-danger">ۇلازىمىتېر zip ھۆججەت شەكلىنى قوللىمايدىكەن</span><?php endif;?>
<?php if(isset($_GET['error_d'])):?><span class="alert alert-danger">زاپاس يوللاش مەغلۇب بولدى</span><?php endif;?>
<?php if(isset($_GET['error_e'])):?><span class="alert alert-danger">خاتا زاپاس ھۆججەت</span><?php endif;?>
<?php if(isset($_GET['error_f'])):?><span class="alert alert-danger">مۇلازىمىتېر zip ھۆججەت شەكلىنى قوللىمايدىكەن</span><?php endif;?>
<?php if(isset($_GET['active_mc'])):?><span class="alert alert-success">قالدۇق يىڭىلاش تامام</span><?php endif;?>
</div>
<div class=line></div>
<form  method="post" action="data.php?action=dell_all_bak" name="form_bak" id="form_bak">
<table class="table table-striped table-bordered table-hover dataTable no-footer">
  <thead>
    <tr>
      <th width="683" colspan="2"><b>زاپاسلانغان ھۆججەت</b></th>
      <th width="226"><b>زاپاسلانغان ۋاقىت</b></th>
      <th width="149"><b>ھۆججەت سىغىمى</b></th>
      <th width="87"></th>
    </tr>
  </head>
  <tbody>
    <?php
        if($bakfiles):
        foreach($bakfiles  as $value):
        $modtime = smartDate(filemtime($value),'Y-m-d H:i:s');
        $size =  changeFileSize(filesize($value));
        $bakname = substr(strrchr($value,'/'),1);
    ?>
    <tr>
      <td width="22"><input type="checkbox" value="<?php echo $value; ?>" name="bak[]" class="ids" /></td>
      <td width="661"><a href="../content/backup/<?php echo $bakname; ?>"><?php echo $bakname; ?></a></td>
      <td><?php echo $modtime; ?></td>
      <td><?php echo $size; ?></td>
      <td><a href="javascript: em_confirm('<?php echo $value; ?>', 'backup', '<?php echo LoginAuth::genToken(); ?>');">كىرگۈزۈش</a></td>
    </tr>
    <?php endforeach;else:?>
      <tr><td class="tdcenter" colspan="5">تېخى زاپاسلانمىغان</td></tr>
    <?php endif;?>
    </tbody>
</table>
<div class="list_footer">
<a href="javascript:void(0);" id="select_all">ھەممىنى تاللاش</a> تاللىغاننى：<a href="javascript:bakact('del');" class="care">ئۆچۈرۈش</a></div>
</form>

<div style="margin:50px 0px 20px 0px;">
    <a href="javascript:$('#import').hide();$('#cache').hide();displayToggle('backup', 0);" style="margin-right: 16px;">ساندان زاپاسلاش +</a> 
    <a href="javascript:$('#backup').hide();$('#cache').hide();displayToggle('import', 0);" style="margin-right: 16px;">يەرلىككە زاپاسلاش +</a> 
    <a href="javascript:$('#backup').hide();$('#import').hide();displayToggle('cache', 0);" style="margin-right: 16px;">قالدۇق يېڭىلاش +</a>
</div>

<form action="data.php?action=bakstart" method="post">
<div id="backup">
    <p>زاپاسلايدىغان ساندان جەدىۋىلىنى تاللاڭ：<br />
        <select multiple="multiple" size="12" name="table_box[]">
        <?php foreach($tables  as $value): ?>
        <option value="<?php echo DB_PREFIX; ?><?php echo $value; ?>" selected="selected"><?php echo DB_PREFIX; ?><?php echo $value; ?></option>
        <?php endforeach; ?>
        </select>
    </p>
    <p>زاپاسلانغان ھۆججەت ساقلاش ئورنى：
        <select name="bakplace" id="bakplace">
            <option value="local" selected="selected">يەرلەك (كومپيوتېرىمدا)</option>
            <option value="server">مۇلازىمىتېردا</option>
        </select>
    </p>
    <p id="local_bakzip">پىرىسلاش (zip شەكلىدە)：<input type="checkbox" style="vertical-align:middle;" value="y" name="zipbak" id="zipbak"></p>
    <p>
        <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
        <input type="submit" value="زاپاسلاش" class="btn btn-primary" />
    </p>
</div>
</form>

<form action="data.php?action=import" enctype="multipart/form-data" method="post">
<div id="import">
    <p class="des">ئوخشاش نەشىردىكى emlog زاپاس ھۆججىتى كىرگۈزۈشكە بۇلىدۇ ،ئاۋال بىر پارچە ساقلاپ قۇيۇڭ<br />ھازىرقى سانلىق مەلۇمات جەدىۋىلى：<?php echo DB_PREFIX; ?></p>
    <p>
        <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
        <input type="file" name="sqlfile" /> <input type="submit" value="ئەكىرىش" class="submit" />
    </p>
</div>
</form>

<div id="cache">
    <p class="des">قالدۇق يېڭىلاش ئارقىلىق بىكەتنىڭ ئېچىلىش سۈرىتىنى، سانداننىڭ ئوقۇپ بىرىش ئىقتىدارىنى ئاشۇرۇشقا بولىدۇ</p>
    <p><input type="button" onclick="window.location='data.php?action=Cache';" value="قالدۇق يېڭىلاش" class="btn btn-primary" /></p>
</div>

<script>
setTimeout(hideActived,2600);
$(document).ready(function(){
    selectAllToggle();
    $("#adm_bakdata_list tbody tr:odd").addClass("tralt_b");
    $("#adm_bakdata_list tbody tr")
        .mouseover(function(){$(this).addClass("trover")})
        .mouseout(function(){$(this).removeClass("trover")});
    $("#bakplace").change(function(){$("#server_bakfname").toggle();$("#local_bakzip").toggle();});
});
function bakact(act){
    if (getChecked('ids') == false) {
        alert('زاپاس ھۆججەتنى تاللاڭ');
        return;
    }
    if(act == 'del' && !confirm('راستىنلا ئۆچۈرەمسىز؟')){return;}
    $("#operate").val(act);
    $("#form_bak").submit();
}
$("#menu_category_sys").addClass('active');
$("#menu_sys").addClass('in');
$("#menu_data").addClass('active');
</script>
