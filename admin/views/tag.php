﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="containertitle"><b>خەتكۈچ باشقۇرۇش</b>
<?php if(isset($_GET['active_del'])):?><span class="alert alert-success">خەتكۈچ ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_edit'])):?><span class="alert alert-success">خەتكۈچ ئۆزگەرتىلدى</span><?php endif;?>
<?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">ئۆچۈرۈدىغان خەتكۈچنى تاللاڭ</span><?php endif;?>
</div>
<div class=line></div>
<form action="tag.php?action=dell_all_tag" method="post" name="form_tag" id="form_tag">
<div>
<li>
<?php 
if($tags):
foreach($tags as $key=>$value): ?>	
<input type="checkbox" name="tag[<?php echo $value['tid']; ?>]" class="ids" value="1" >
<a href="tag.php?action=mod_tag&tid=<?php echo $value['tid']; ?>"><?php echo $value['tagname']; ?></a> &nbsp;&nbsp;&nbsp;
<?php endforeach; ?>
</li>
<input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
<li style="margin:20px 0px">
<a href="javascript:void(0);" id="select_all">ھەممىنى تاللاش</a> تاللىغاننى：
<a href="javascript:deltags();" class="care">ئۆچۈرۈش</a>
</li>
<?php else:?>
<li style="margin:20px 30px">تېخى خەتكۈچ يوق ، خەتكۈچنى يازما يازغان ۋاقتىڭىزدا قوشسىڭىز بولىدۇ</li>
<?php endif;?>
</div>
</form>
<script>
selectAllToggle();
function deltags(){
	if (getChecked('ids') == false) {
		alert('خەتكۈچنى تاللاڭ');
		return;
	}
	if(!confirm('راستىنلا ئۆچۈرەمسىز؟')){return;}
	$("#form_tag").submit();
}
setTimeout(hideActived,2600);
$("#menu_tag").addClass('active');
</script>
