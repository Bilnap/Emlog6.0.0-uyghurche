﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="containertitle"><b>ئەزا تەڭشەش</b>
<?php if(isset($_GET['error_login'])):?><span class="alert alert-danger">ئەزا نامى بوش قالمىسۇن</span><?php endif;?>
<?php if(isset($_GET['error_exist'])):?><span class="alert alert-danger">بۇ ئىسىم مەۋجۈت</span><?php endif;?>
<?php if(isset($_GET['error_pwd_len'])):?><span class="alert alert-danger">پارول 6 خانىدىن يۇقىرى بولسۇن</span><?php endif;?>
<?php if(isset($_GET['error_pwd2'])):?><span class="alert alert-danger">قايتىلانما پارول بىردەك ئەمەس</span><?php endif;?>
</div>
<div class=line></div>
<form action="user.php?action=update" method="post">
<div class="form-group">
	<li><input type="text" value="<?php echo $username; ?>" name="username" style="width:200px;" class="form-control" /> ئەزا نامى</li>
	<li><input type="text" value="<?php echo $nickname; ?>" name="nickname" style="width:200px;" class="form-control" /> ئىسىم</li>
	<li><input type="password" value="" name="password" style="width:200px;" class="form-control" /> يېڭى پارول (ئۆزگەرتمىسىڭىز بوش قويۇڭ)</li>
	<li><input type="password" value="" name="password2" style="width:200px;" class="form-control" /> قايتىلانما پارول</li>
	<li><input type="text"  value="<?php echo $email; ?>" name="email" style="width:200px;" class="form-control" /> ئېلخەت</li>
    <li>
	<select name="role" id="role" class="form-control">
		<option value="writer" <?php echo $ex1; ?>>ئاپتور</option>
		<option value="admin" <?php echo $ex2; ?>>باشقۇرغۇچى</option>
	</select>
	</li>
    <li id="ischeck">
	<select name="ischeck" class="form-control">
        <option value="n" <?php echo $ex3; ?>>ئەسىرى تەستىقلانمىسۇن</option>
		<option value="y" <?php echo $ex4; ?>>ئەسىرى تەستىقلانسۇن</option>
	</select>
	</li>
	<li>چۈشەندۈرىلىش<br />
	<textarea name="description" rows="5" style="width:260px;" class="form-control"><?php echo $description; ?></textarea></li>
	<li>
    <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
	<input type="hidden" value="<?php echo $uid; ?>" name="uid" />
	<input type="submit" value="ساقلاش" class="btn btn-primary" />
	<input type="button" value="قالدۇرۇش" class="btn btn-default" onclick="window.location='user.php';" /></li>
</div>
</form>
<script>
setTimeout(hideActived,2600);
$("#menu_user").addClass('active');
if($("#role").val() == 'admin') $("#ischeck").hide();
$("#role").change(function(){$("#ischeck").toggle()})
</script>
