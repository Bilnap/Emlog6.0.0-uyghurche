﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if (!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="panel-heading">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="./configure.php">ئاساسى تەڭشەڭ</a></li>
        <li role="presentation"><a href="./seo.php">SEOتەڭشەك</a></li>
        <li role="presentation"><a href="./blogger.php">ئەزا تەڭشەش</a></li>
        <?php if (isset($_GET['activated'])): ?><span class="alert alert-success">ساقلاندى</span><?php endif; ?>
    </ul>
</div>
<div class="panel-body" style="margin-left:30px;">
    <form action="configure.php?action=mod_config" method="post" name="input" id="input">
        <div class="form-group">
            <label>ئىكەت ئسىمى：</label><input style="width:390px;" class="form-control" value="<?php echo $blogname; ?>" name="blogname" />
        </div>
        <div class="form-group">
            <label>بىكەت قوشۇمچە ئىسمى：</label><textarea name="bloginfo" cols="" rows="3" style="width:386px;" class="form-control"><?php echo $bloginfo; ?></textarea>
        </div>
        <div class="form-group">
            <label>بىكەت ئادرىسى：</label><input style="width:390px;" class="form-control" value="<?php echo $blogurl; ?>" name="blogurl" />
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="y" name="detect_url" id="detect_url" <?php echo $conf_detect_url; ?> />ئاپتۇماتىك بىكەت ئادرىسىنى تەكشۈرۈش
                </label>
            </div>
        </div>
        <div class="form-group form-inline">
            <label>ھەر بىر بەتتە كۆرۈنۈش سانى </label><input style="width:50px;" class="form-control" value="<?php echo $index_lognum; ?>" name="index_lognum" />پارچە
        </div>
        <div class="form-group form-inline">
            <label>تۇرۇشلۇق ئورنىڭىزدىكى ۋاقىت：</label>
            <select name="timezone" style="width:320px;" class="form-control">
                <?php
                foreach ($tzlist as $key => $value):
                    $ex = $key == $timezone ? "selected=\"selected\"" : '';
                    ?>
                    <option value="<?php echo $key; ?>" <?php echo $ex; ?>><?php echo $value; ?></option>
                <?php endforeach; ?>
            </select>
            (يەرلىك ۋاقىت：<?php echo date('Y-m-d H:i:s'); ?>)
        </div>
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="y" name="login_code" id="login_code" <?php echo $conf_login_code; ?> />كىرىش تەستىق كودى
                </label>
            </div>
            <div class="checkbox form-inline">
                <label><input type="checkbox" value="y" name="isexcerpt" id="isexcerpt" <?php echo $conf_isexcerpt; ?> />ئۈزۈندە ئېلىش</label>，
                يازما ئالدىدىكى<input type="text" name="excerpt_subnum" value="<?php echo Option::get('excerpt_subnum'); ?>" class="form-control" style="width:60px;" />خەت ئېلىنسۇن
            </div>          
        </div>
        <div class="form-group form-inline">
            RSSچىقىرىش <input maxlength="5" style="width:50px;" value="<?php echo $rss_output_num; ?>" class="form-control" name="rss_output_num" /> يازما（0ئۈچۈن تاقالدى），ئۈستىگە چىقىرىش
            <select name="rss_output_fulltext" class="form-control">
                <option value="y" <?php echo $ex1; ?>>پۈتۈن يازما</option>
                <option value="n" <?php echo $ex2; ?>>ئۈزۈندە</option>
            </select>
        </div>
        <div class="form-group">
            <div class="checkbox form-inline">
                <label><input type="checkbox" value="y" name="iscomment" id="iscomment" <?php echo $conf_iscomment; ?> />ئىنكاسنى ئېچىش</label>，ئىنكاش يوللاش ۋاقىت پەرىقى<input maxlength="5" style="width:50px;" class="form-control" value="<?php echo $comment_interval; ?>" name=comment_interval />سىكونت
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="y" name="ischkcomment" id="ischkcomment" <?php echo $conf_ischkcomment; ?> />ئىنكاس تەستىقلانسۇن
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="y" name="comment_code" id="comment_code" <?php echo $conf_comment_code; ?> />ئىنكاس تەستىق كودى
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="y" name="isgravatar" id="isgravatar" <?php echo $conf_isgravatar; ?> />ئىنكاسچى باش سۆرىتى
                </label>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="y" name="comment_needchinese" id="comment_needchinese" <?php echo $conf_comment_needchinese; ?> />ئىنكاس ئارىسىدا چوقۇم خەنزۇچە خەت بولسۇن
                </label>
            </div>
            <div class="checkbox form-inline">
                <label><input type="checkbox" value="y" name="comment_paging" id="comment_paging" <?php echo $conf_comment_paging; ?> />ئىنكاسقا بەت ئايرىش，</label>
                ھەربىت بەتتە<input maxlength="5" style="width:50px;" class="form-control" value="<?php echo $comment_pnum; ?>" name="comment_pnum" />تال ئىنكاس كۆرۈنسۇن，
                <select name="comment_order" class="form-control"><option value="newer" <?php echo $ex3; ?>>يېڭىلىنىشقا</option><option value="older" <?php echo $ex4; ?>>كونىغا قارىتا</option></select>قارىتا ئالدىغان تىزىلسۇن
            </div>
        </div>
        <div class="form-group form-inline">
            <input maxlength="10" style="width:80px;" class="form-control" value="<?php echo $att_maxsize; ?>" name="att_maxsize" /> KB，قوشۇمچە يوللاش ئەڭ چوڭ چېكى
        </div>
        <div class="form-group form-inline">
            <input maxlength="200" style="width:320px;" class="form-control" value="<?php echo $att_type; ?>" name="att_type" /> مۇشۇلارنى قوللايدۇ (كۆپ بولسا بەش بىلەن ئايرىپ يېزىڭ)
        </div>
        <div class="form-group form-inline">
            <input type="checkbox" value="y" name="isthumbnail" id="isthumbnail" <?php echo $conf_isthumbnail; ?> />رەسىم قوشۇمچىسى كىچىكلىتىش， چوڭلۇقى：<input maxlength="5" style="width:60px;" class="form-control" value="<?php echo $att_imgmaxw; ?>" name="att_imgmaxw" /> x <input maxlength="5" style="width:60px;" class="form-control" value="<?php echo $att_imgmaxh; ?>" name="att_imgmaxh" />（بىرلىكى：پىكسىل）
        </div>
        <div class="form-group">
            ICPئەن نۇمۇرى：
            <input maxlength="200" style="width:390px;" class="form-control" value="<?php echo $icp; ?>" name="icp" />
        </div>
        <div class="form-group">
            <label>باشبەت ئاستى ئۇچۇر (html قوللايدۇ، سىتاستىكا كودى قوشىۋالسىڭىز بولىدۇ)</label>
            <textarea name="footer_info" cols="" rows="6" class="form-control" style="width:386px;"><?php echo $footer_info; ?></textarea>
        </div>
        <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
        <input type="submit" value="ساقلاش" class="btn btn-primary" />
    </form>
</div>
<script>
    setTimeout(hideActived, 2600);
    $("#menu_category_sys").addClass('active');
    $("#menu_sys").addClass('in');
    $("#menu_setting").addClass('active');
</script>
