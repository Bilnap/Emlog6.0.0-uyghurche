﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<script>setTimeout(hideActived,2600);</script>
<div class="panel-heading">
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="./configure.php">ئاساسىي تەڭشەك</a></li>
    <li role="presentation" class="active"><a href="./seo.php">SEOتەڭشەك</a></li>
    <li role="presentation"><a href="./blogger.php">ئەزا تەڭشەش</a></li>
    <?php if(isset($_GET['activated'])):?><span class="alert alert-success">ساقلاندى</span><?php endif;?>
    <?php if(isset($_GET['error'])):?><span class="alert alert-danger">ساقلانمىدى: تۈۋەندىكى ھۆججەتكە .htaccess يېزىش ھوقۇقى بىرىلمىگەن</span><?php endif;?>
</ul>
</div>
<div class="panel-body" style="margin-left:30px;">
<form action="seo.php?action=update" method="post">
<h4>يازما ئۇلىنىش تەڭشىكى</h4>
<div class="alert alert-info" style="width: 100%">
       بۇ ئىقتىدار ئارقىلىق سىز ئەسەر مەزمۇن بىتىنىڭ ئادىرىسىنى خالىغانچە ئۆزگەرتەلەيسىز. نۇرمال ئەھۋالدا ئەسەر ئادىرىسى باشقا ئىسىم ئىقتىدارىنى تاقاپ قويۇڭ. 
    <br />ئۇلىنىش باشقا ئىسىم ئىقتىدارىنى ئېچىۋەتسىڭىز بەت يۈزى ئۇلىنىش قىلالايسىز
</div>
<div class="form-group">
            <div class="radio">
                <label>
                    <input type="radio" name="permalink" value="0" <?php echo $ex0; ?>>سۈكۈتتىكى شەكىل：<span class="permalink_url"><?php echo BLOG_URL; ?>?post=1</span>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="permalink" value="1" <?php echo $ex1; ?>>ھۆججەت شەكلى：<span class="permalink_url"><?php echo BLOG_URL; ?>post-1.html</span>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="permalink" value="2" <?php echo $ex2; ?>>مۇندەرىجە شەكلى：<span class="permalink_url"><?php echo BLOG_URL; ?>post/1</span>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="permalink" value="3" <?php echo $ex3; ?>>تۈر شەكلى：<span class="permalink_url"><?php echo BLOG_URL; ?>category/1.html</span>
                </label>
            </div>
</div>
<div class="form-group">
                <div class="checkbox">
                <label>
                    <input type="checkbox" style="vertical-align:middle;" value="y" name="isalias" id="isalias" <?php echo $isalias; ?> />ئەسەر باشقا ئىسىم ئىقتىدارىنى قوزغىتىش
                </label>
            </div>
                <div class="checkbox">
                <label>
                    <input type="checkbox" style="vertical-align:middle;" value="y" name="isalias_html" id="isalias_html" <?php echo $isalias_html; ?> />ئەسەر باشقا ئىسىم html ئارقا قۇشۇمچە ئىقتىدارىنى قوزغىتىش
                </label>
            </div>
</div>

<h4>meta تەڭشىكى</h4>
<div class="form-group">
    <li>
        <label>بىكەت تور كۆرگۈچتىكى ئىسمى(title)</label>
        <input maxlength="200" style="width:300px;" class="form-control" value="<?php echo $site_title; ?>" name="site_title" />
    </li>
    <li>
        <label>بىكەت ھالقىلىق  سۆزلىرى(keywords)</label>
        <input maxlength="200" style="width:300px;" class="form-control" value="<?php echo $site_key; ?>" name="site_key" />
    </li>
    <li>
        <label>بىكەت توركۆرگۈچ چۈشەندۈرلىشى(description)</label>
        <textarea name="site_description" class="form-control" cols="" rows="4" style="width:300px;"><?php echo $site_description; ?></textarea>
    </li>
    <li>
        <label>ئەسەر توركۆرگۈچ تېما شەكلى：</label>
        <select name="log_title_style" class="form-control" style="width: 120px;">
        <option value="0" <?php echo $opt0; ?>>ئەسەر تېمىسى</option>
        <option value="1" <?php echo $opt1; ?>>ئەسەر تېمىسى - بىكەت ئىسمى</option>
        <option value="2" <?php echo $opt2; ?>>ئەسەر تېمىسى - توركۆرگۈچتىكى ئىسمى</option>
        </select>
    </li>
    <li style="margin-top:10px;">
        <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
        <input type="submit" value="ساقلاش" class="btn btn-primary" />
    </li>
</div>
</form>
</div>
<script>
    setTimeout(hideActived, 2600);
    $("#menu_category_sys").addClass('active');
    $("#menu_sys").addClass('in');
    $("#menu_setting").addClass('active');
</script>