<?php if (!defined('EMLOG_ROOT')) {exit('error!');}?>
<script>setTimeout(hideActived, 2600);</script>
<div class="containertitle"><b>باشقۇرۇش بىتى</b></div>
<?php doAction('adm_main_top'); ?>
<div class="row">
<?php if (ROLE == ROLE_ADMIN): ?>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-laptop fa-fw"></i> بىكەت ئۇچۇرى
                </div>
                <div class="panel-body" id="admindex_servinfo">
                    <ul>
                        <li>تال<b><?php echo $sta_cache['lognum'];?></b>يازما，<b><?php echo $sta_cache['comnum_all'];?></b>تال ئىنكاس</li>
                        <li>ساندان ئالدى قۇشۇمچىسى：<?php echo DB_PREFIX; ?></li>
                        <li>PHPنەشىرى：<?php echo $php_ver; ?></li>
                        <li>MySQLنەشىرى：<?php echo $mysql_ver; ?></li>
                        <li>مۇلازىمىتېر مۇھىتى：<?php echo $serverapp; ?></li>
                        <li>مۇلازىمىتېرغا يۈكلەشكە بولدىغان ئەڭ چوڭ ھۆججەت：<?php echo $uploadfile_maxsize; ?></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-volume-down fa-fw"></i> ئورگان ئۇچۇرلىرى
                </div>
                <div class="panel-body" id="admindex_msg">
                    <ul></ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div id="admindex">
                <div id="about" class="alert alert-warning">
                  ئىشلىتىشىڭىزنى قارشى ئالىمىز &copy; <a href="http://www.emlog.net" target="_blank">emlog</a>-<a href="http://www.bashqut.com">Bashqut Blog</a> v<?php echo Option::EMLOG_VERSION; ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#admindex_msg ul").html("<span class=\"ajax_remind_1\">ئوقۇۋاتىدۇ...</span>");
            $.getJSON("<?php echo OFFICIAL_SERVICE_HOST; ?>services/messenger.php?v=<?php echo Option::EMLOG_VERSION; ?>&callback=?",
                    function(data) {
                        $("#admindex_msg ul").html("");
                        $.each(data.items, function(i, item) {
                            var image = '';
                            if (item.image != '') {
                                image = "<a href=\"" + item.url + "\" target=\"_blank\" title=\"" + item.title + "\"><img src=\"" + item.image + "\"></a><br />";
                            }
                            $("#admindex_msg ul").append("<li class=\"msg_type_" + item.type + "\">" + image + "<span>" + item.date + "</span><a href=\"" + item.url + "\" target=\"_blank\">" + item.title + "</a></li>");
                        });
                    });
        });
    </script>
<?php else: ?>
<div class="row">
        <div class="col-lg-12">
            <div id="admindex_main">
                <div id="about"><a href="blogger.php"><?php echo $name; ?></a> （<b><?php echo $sta_cache[UID]['lognum']; ?></b>پارچە يازما，<b><?php echo $sta_cache[UID]['commentnum']; ?></b>تال ئىنكاس）</div>
            </div>
            <div class="clear"></div>
        </div>
</div>
<?php endif; ?>
