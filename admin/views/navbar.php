﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');} ?>
<div class="containertitle"><b>يېتەكلىگۈچ باشقۇرۇش</b>
<?php if(isset($_GET['active_taxis'])):?><span class="alert alert-success">تەرتىپ يېڭىلاندى</span><?php endif;?>
<?php if(isset($_GET['active_del'])):?><span class="alert alert-success">يىتەكلىگۈچ ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_edit'])):?><span class="alert alert-success">يىتەكلىگۈچ ئۆزگەرتىلدى</span><?php endif;?>
<?php if(isset($_GET['active_add'])):?><span class="alert alert-success">يىتەكلىگۈچ قوشۇلدى</span><?php endif;?>
<?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">يىتەكلىگۈچ ئىسمى ۋە ئاسرىسى بوش قالمىسۇن</span><?php endif;?>
<?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">تەرتىپ تىزغۇدەك يىتەكلىگۈچ يوق</span><?php endif;?>
<?php if(isset($_GET['error_c'])):?><span class="alert alert-danger">سۈكۈتتىكى يىتەكلىگۈچنى ئۆچۈرگىلى بولمايدۇ</span><?php endif;?>
<?php if(isset($_GET['error_d'])):?><span class="alert alert-danger">قوشماقچى بولغان تۈرنى تاللاڭ</span><?php endif;?>
<?php if(isset($_GET['error_e'])):?><span class="alert alert-danger">قوشماقچى بولغان بەت يۈزىنى تاللاڭ</span><?php endif;?>
<?php if(isset($_GET['error_f'])):?><span class="alert alert-danger">يول باشلاش ئادىرىسىدا مەسلە بار (http:// نى قوشۇپ يېزىڭ)</span><?php endif;?>
</div>
<form action="navbar.php?action=taxis" method="post">
  <table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
      <tr>
        <th width="50"><b>تەرتىپ</b></th>
        <th width="230"><b>يىتەكلىگۈچ</b></th>
        <th width="60" class="tdcenter"><b>تۈر</b></th>
        <th width="60" class="tdcenter"><b>ھالەت</b></th>
        <th width="50" class="tdcenter"><b>كۆرۈش</b></th>
        <th width="360"><b>ئادرىس</b></th>
        <th width="100"></th>
      </tr>
    </thead>
    <tbody>
    <?php 
    if($navis):
    foreach($navis as $key=>$value):
        if ($value['pid'] != 0) {
            continue;
        }
        $value['type_name'] = '';
        switch ($value['type']) {
            case Navi_Model::navitype_home:
            case Navi_Model::navitype_t:
            case Navi_Model::navitype_admin:
                $value['type_name'] = 'سېستىما';
                break;
            case Navi_Model::navitype_sort:
                $value['type_name'] = '<font color="blue">تۈر</font>';
                break;
            case Navi_Model::navitype_page:
                $value['type_name'] = '<font color="#00A3A3">بەت يۈزى</font>';
                break;
            case Navi_Model::navitype_custom:
                $value['type_name'] = '<font color="#FF6633">بەلگىلەش</font>';
                break;
        }
        doAction('adm_navi_display');
    
    ?>  
      <tr>
        <td><input class="form-control em-small" name="navi[<?php echo $value['id']; ?>]" value="<?php echo $value['taxis']; ?>" maxlength="4" /></td>
        <td><a href="navbar.php?action=mod&amp;navid=<?php echo $value['id']; ?>" title="يىتەكلىگۈچ تەھرىرلەش"><?php echo $value['naviname']; ?></a></td>
        <td class="tdcenter"><?php echo $value['type_name'];?></td>
        <td class="tdcenter">
        <?php if ($value['hide'] == 'n'): ?>
        <a href="navbar.php?action=hide&amp;id=<?php echo $value['id']; ?>" title="بېسىپ يۇشۇرۇڭ">ئاشكارە</a>
        <?php else: ?>
        <a href="navbar.php?action=show&amp;id=<?php echo $value['id']; ?>" title="بېسىپ كۆرستىڭ" style="color:red;">يۇشۇرۇن</a>
        <?php endif;?>
        </td>
        <td class="tdcenter">
        <a href="<?php echo $value['url']; ?>" target="_blank">
        <img src="./views/images/<?php echo $value['newtab'] == 'y' ? 'vlog.gif' : 'vlog2.gif';?>" align="absbottom" border="0" /></a>
        </td>
        <td><?php echo $value['url']; ?></td>
        <td>
        <a href="navbar.php?action=mod&amp;navid=<?php echo $value['id']; ?>">تەھرىرلەش</a>
        <?php if($value['isdefault'] == 'n'):?>
        <a href="javascript: em_confirm(<?php echo $value['id']; ?>, 'navi', '<?php echo LoginAuth::genToken(); ?>');" class="care">ئۆچۈرۈش</a>
        <?php endif;?>
        </td>
      </tr>
    <?php
        if(!empty($value['childnavi'])):
        foreach ($value['childnavi'] as $val):
    ?>
        <tr>
        <td><input class="form-control em-small" name="navi[<?php echo $val['id']; ?>]" value="<?php echo $val['taxis']; ?>" maxlength="4" /></td>
        <td>---- <a href="navbar.php?action=mod&amp;navid=<?php echo $val['id']; ?>" title="يىتەكلىگۈچ تەھرىرلەش"><?php echo $val['naviname']; ?></a></td>
        <td class="tdcenter"><?php echo $value['type_name'];?></td>
        <td class="tdcenter">
        <?php if ($val['hide'] == 'n'): ?>
        <a href="navbar.php?action=hide&amp;id=<?php echo $val['id']; ?>" title="بېسىپ يۇشۇرۇڭ">ئاشكارە</a>
        <?php else: ?>
        <a href="navbar.php?action=show&amp;id=<?php echo $val['id']; ?>" title="چىقىپ كۆرسىتىڭ" style="color:red;">يۇشۇرۇن</a>
        <?php endif;?>
        </td>
        <td class="tdcenter">
        <a href="<?php echo $val['url']; ?>" target="_blank">
        <img src="./views/images/<?php echo $val['newtab'] == 'y' ? 'vlog.gif' : 'vlog2.gif';?>" align="absbottom" border="0" /></a>
        </td>
        <td><?php echo $val['url']; ?></td>
        <td>
        <a href="navbar.php?action=mod&amp;navid=<?php echo $val['id']; ?>">تەسرىرلەش</a>
        <?php if($val['isdefault'] == 'n'):?>
        <a href="javascript: em_confirm(<?php echo $val['id']; ?>, 'navi', '<?php echo LoginAuth::genToken(); ?>');" class="care">ئۆچۈرۈش</a>
        <?php endif;?>
        </td>
      </tr>
      <?php endforeach;endif; ?>
    <?php endforeach;else:?>
      <tr><td class="tdcenter" colspan="4">تېخى يىتەكلىگۈچ قوشماپسىز</td></tr>
    <?php endif;?>
    </tbody>
  </table>
  <div class="list_footer"><input type="submit" value="تەرتىپ ئۆزگەرتىش" class="btn btn-primary" /></div>
</form>
<div id="row" style="margin-top: 30px;">
    <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    ئۆزى يىتەكلىگۈچ قوشۇش
                </div>
                <div class="panel-body" id="admindex_servinfo">
                    <form action="navbar.php?action=add" method="post" name="navi" id="navi">
                    <ul>
                        <li><input maxlength="4" class="form-control" placeholder="تەرتىپ" name="taxis" /></li>
                        <li><input class="form-control" name="naviname" placeholder="يىتەكلىگۈچ ئىسمى" /></li>
                        <li><input maxlength="200" class="form-control" placeholder="ئادرىس (http بولسۇن)" name="url" id="url" /></li>
                        <li class="form-inline">
                            <select name="pid" id="pid" class="form-control">
                                <option value="0">يوق</option>
                                <?php
                                    foreach($navis as $key=>$value):
                                        if($value['type'] != Navi_Model::navitype_custom || $value['pid'] != 0) {
                                            continue;
                                        }
                                ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['naviname']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            ئاتا يىتەكلىگۈچ
                        </li>
                        <li class="form-inline"><input type="checkbox" style="vertical-align:middle;" class="form-control" value="y" name="newtab" /> يېڭى كۆزنەكتە ئېچىلسۇن</li>
                        <li><input type="submit" class="btn btn-primary" name="" value="قوشۇش"  /></li>
                    </ul>
                    </form>
                </div>
                </div>
            </div>
        </div>
    <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    تۈرنى يىتەكلىگۈچكە قوشۇش
                </div>
                <div class="panel-body" id="admindex_servinfo">
                    <form action="navbar.php?action=add_sort" method="post" name="navi" id="navi">
                    <ul>
                    <?php 
                    if($sorts):
                    foreach($sorts as $key=>$value):
                    if ($value['pid'] != 0) {
                        continue;
                    }
                    ?>
                    <li>
                        <input type="checkbox" style="vertical-align:middle;" name="sort_ids[]" value="<?php echo $value['sid']; ?>" class="ids" />
                        <?php echo $value['sortname']; ?>
                    </li>
                    <?php
                        $children = $value['children'];
                        foreach ($children as $key):
                        $value = $sorts[$key];
                    ?>
                    <li>
                        &nbsp; &nbsp; &nbsp;  <input type="checkbox" style="vertical-align:middle;" name="sort_ids[]" value="<?php echo $value['sid']; ?>" class="ids" />
                        <?php echo $value['sortname']; ?>
                    </li>
                    <?php 
                        endforeach;
                   endforeach;
                   ?>
                    <li><input type="submit" name="" class="btn btn-primary" value="قوشۇش" /></li>
                    <?php else:?>
                    <li>تېخى تۈر يوق，<a href="sort.php">تۈر قوشۇش</a></li>
                    <?php endif;?> 
                    </ul>
                    </form>
                </div>
            </div>
        </div>
    <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    بەت يۈزىنى يىتەكلىگۈچكە قوشۇش
                </div>
                <div class="panel-body" id="admindex_servinfo">
                    <form action="navbar.php?action=add_page" method="post" name="navi" id="navi">
                    <ul>
                    <?php 
                        if($pages):
                        foreach($pages as $key=>$value): 
                        ?>
                        <li>
                            <input type="checkbox" style="vertical-align:middle;" name="pages[<?php echo $value['gid']; ?>]" value="<?php echo $value['title']; ?>" class="ids" />
                            <?php echo $value['title']; ?>
                        </li>
                        <?php endforeach;?>
                        <li><input type="submit" class="btn btn-primary" name="" value="قوشۇش"  /></li>
                        <?php else:?>
                        <li>تېخى بەت يۈزى يوق，<a href="page.php">بەت يۈزى قوشۇش</a></li>
                    <?php endif;?>
                    </ul>
                    </form>
                </div>
            </div>
        </div>
</div>

<script>
$("#navi_add_custom").css('display', $.cookie('em_navi_add_custom') ? $.cookie('em_navi_add_custom') : '');
$("#navi_add_sort").css('display', $.cookie('em_navi_add_sort') ? $.cookie('em_navi_add_sort') : '');
$("#navi_add_page").css('display', $.cookie('em_navi_add_page') ? $.cookie('em_navi_add_page') : '');
$(document).ready(function(){
    $("#adm_navi_list tbody tr:odd").addClass("tralt_b");
    $("#adm_navi_list tbody tr")
        .mouseover(function(){$(this).addClass("trover")})
        .mouseout(function(){$(this).removeClass("trover")})
});
setTimeout(hideActived, 2600);
$("#menu_category_view").addClass('active');
$("#menu_view").addClass('in');
$("#menu_navi").addClass('active');
</script>
