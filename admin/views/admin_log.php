﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php
if(!defined('EMLOG_ROOT')) {exit('error!');}
$isdraft = $pid == 'draft' ? '&pid=draft' : '';
$isDisplaySort = !$sid ? "style=\"display:none;\"" : '';
$isDisplayTag = !$tagId ? "style=\"display:none;\"" : '';
$isDisplayUser = !$uid ? "style=\"display:none;\"" : '';
?>
<div class="panel-heading">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" <?php if ($pid != 'draft'){echo 'class="active"';}?>><a href="admin_log.php">يازما باشقۇرۇش</a></li>
        <li role="presentation" <?php if ($pid == 'draft'){echo 'class="active"';}?>><a href="admin_log.php?pid=draft">ئورگىنال</a></li>
        <?php if(isset($_GET['active_del'])):?><span class="alert alert-success">ئۆچۈرۈلدى</span><?php endif;?>
        <?php if(isset($_GET['active_up'])):?><span class="alert alert-success">چوققىلاندى</span><?php endif;?>
        <?php if(isset($_GET['active_down'])):?><span class="alert alert-success">چوققىسىزلاندى</span><?php endif;?>
        <?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">ئەسەرنى تاللاڭ</span><?php endif;?>
        <?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">مەشغۇلاتنى تاللاڭ</span><?php endif;?>
        <?php if(isset($_GET['active_post'])):?><span class="alert alert-success">يوللاندى</span><?php endif;?>
        <?php if(isset($_GET['active_move'])):?><span class="alert alert-success">يۆتكەلدى</span><?php endif;?>
        <?php if(isset($_GET['active_change_author'])):?><span class="alert alert-success">ئاپتور ئۆزگەرتىلدى</span><?php endif;?>
        <?php if(isset($_GET['active_hide'])):?><span class="alert alert-success">ئورگىنالغا يۆتكەلدى</span><?php endif;?>
        <?php if(isset($_GET['active_savedraft'])):?><span class="alert alert-success">ئورگىنالغا ساقلاندى</span><?php endif;?>
        <?php if(isset($_GET['active_savelog'])):?><span class="alert alert-success">ساقلاندى</span><?php endif;?>
        <?php if(isset($_GET['active_ck'])):?><span class="alert alert-success">تەستىقلاندى</span><?php endif;?>
        <?php if(isset($_GET['active_unck'])):?><span class="alert alert-success">ئەسەر رەت قىلىندى</span><?php endif;?>
    </ul>
</div>
<div style="margin: 0px 15px;">
<div class=line></div>
<div class="filters">
<div id="f_title" class="form-inline">
    <div style="float:left; margin-top:8px;">
        <span id="f_t_sort">
            <select name="bysort" id="bysort" onChange="selectSort(this);" style="width:120px;" class="form-control">
            <option value="" selected="selected">تۈر بويىچە...</option>
            <?php 
            foreach($sorts as $key=>$value):
            if ($value['pid'] != 0) {
                continue;
            }
            $flg = $value['sid'] == $sid ? 'selected' : '';
            ?>
            <option value="<?php echo $value['sid']; ?>" <?php echo $flg; ?>><?php echo $value['sortname']; ?></option>
            <?php
                $children = $value['children'];
                foreach ($children as $key):
                $value = $sorts[$key];
                $flg = $value['sid'] == $sid ? 'selected' : '';
            ?>
            <option value="<?php echo $value['sid']; ?>" <?php echo $flg; ?>>&nbsp; &nbsp; &nbsp; <?php echo $value['sortname']; ?></option>
            <?php
            endforeach;
            endforeach;
            ?>
            <option value="-1" <?php if($sid == -1) echo 'selected'; ?>>تۈرسىز</option>
            </select>
        </span>
        <?php if (ROLE == ROLE_ADMIN && count($user_cache) > 1):?>
        <span id="f_t_user">
            <select name="byuser" id="byuser" onChange="selectUser(this);" style="width:120px;" class="form-control">
                <option value="" selected="selected">ئاپتور بويىچە...</option>
                <?php 
                foreach($user_cache as $key=>$value):
                $flg = $key == $uid ? 'selected' : '';
                ?>
                <option value="<?php echo $key; ?>" <?php echo $flg; ?>><?php echo $value['name']; ?></option>
                <?php
                endforeach;
                ?>
            </select>
        </span>
        <?php endif;?>
        <span id="f_t_tag"><a href="javascript:void(0);">خەتكۈچ بويىچە</a></span>
    </div>
    <div style="float:right;">
        <form action="admin_log.php" method="get">
            <input type="text" name="keyword" class="form-control" placeholder="يازما ئىزدەش">
        <?php if($pid):?>
        <input type="hidden" id="pid" name="pid" value="draft">
        <?php endif;?>
        </form>
    </div>
    <div style="clear:both"></div>
</div>
<div id="f_tag" <?php echo $isDisplayTag ?>>
    خەتكۈچ：
    <?php 
    if(empty($tags)) echo 'خەتكۈچ يوق';
    foreach($tags as $val):
        $a = 'tag_'.$val['tid'];
        $$a = '';
        $b = 'tag_'.$tagId;
        $$b = "class=\"filter\"";
    ?>
    <span <?php echo $$a; ?>><a href="./admin_log.php?tagid=<?php echo $val['tid'].$isdraft; ?>"><?php echo $val['tagname']; ?></a></span>
    <?php endforeach;?>
</div>
</div>
<form action="admin_log.php?action=operate_log" method="post" name="form_log" id="form_log">
  <input type="hidden" name="pid" value="<?php echo $pid; ?>">
  <table class="table table-striped table-bordered table-hover dataTable no-footer">
  <thead>
      <tr>
        <th width="511" colspan="2"><b>تېما</b></th>
        <?php if ($pid != 'draft'): ?>
        <th width="50" class="tdcenter"><b>كۆرۈش</b></th>
        <?php endif; ?>
        <th width="100"><b>ئاپتور</b></th>
        <th width="146"><b>تۈر</b></th>
        <th width="130"><b><a href="./admin_log.php?sortDate=<?php echo $sortDate.$sorturl; ?>">ۋاقىت</a></b></th>
        <th width="49" class="tdcenter"><b><a href="./admin_log.php?sortComm=<?php echo $sortComm.$sorturl; ?>">ئىنكاس</a></b></th>
        <th width="59" class="tdcenter"><b><a href="./admin_log.php?sortView=<?php echo $sortView.$sorturl; ?>">كۆرۈلىشى</a></b></th>
      </tr>
    </thead>
    <tbody>
    <?php
    if($logs):
    foreach($logs as $key=>$value):
    $sortName = $value['sortid'] == -1 && !array_key_exists($value['sortid'], $sorts) ? 'تۈريوق' : $sorts[$value['sortid']]['sortname'];
    $author = $user_cache[$value['author']]['name'];
    $author_role = $user_cache[$value['author']]['role'];
    ?>
      <tr>
      <td width="21"><input type="checkbox" name="blog[]" value="<?php echo $value['gid']; ?>" class="ids" /></td>
      <td width="490"><a href="write_log.php?action=edit&gid=<?php echo $value['gid']; ?>"><?php echo $value['title']; ?></a>
      <?php if($value['top'] == 'y'): ?><img src="./views/images/top.png" align="top" title="چوققا" /><?php endif; ?>
      <?php if($value['sortop'] == 'y'): ?><img src="./views/images/sortop.png" align="top" title="تۈر" /><?php endif; ?>
      <?php if($value['attnum'] > 0): ?><img src="./views/images/att.gif" align="top" title="قوشۇمچە：<?php echo $value['attnum']; ?>" /><?php endif; ?>
      <?php if($pid != 'draft' && $value['checked'] == 'n'): ?><sapn style="color:red;"> - تەستىقسىز</sapn><?php endif; ?>
      <span style="display:none; margin-left:8px;">
        <?php if($pid != 'draft' && ROLE == ROLE_ADMIN && $value['checked'] == 'n'): ?>
        <a href="./admin_log.php?action=operate_log&operate=check&gid=<?php echo $value['gid']?>&token=<?php echo LoginAuth::genToken(); ?>">تەستىقلاش</a> 
        <?php elseif($pid != 'draft' && ROLE == ROLE_ADMIN && $author_role == ROLE_WRITER):?>
        <a href="./admin_log.php?action=operate_log&operate=uncheck&gid=<?php echo $value['gid']?>&token=<?php echo LoginAuth::genToken(); ?>">قالدۇرۇش</a> 
        <?php endif;?>
      </span>
      </td>
      <?php if ($pid != 'draft'): ?>
      <td class="tdcenter">
      <a href="<?php echo Url::log($value['gid']); ?>" target="_blank" title="يېڭى كۆزنەكتە كۆرۈش">
      <img src="./views/images/vlog.gif" align="absbottom" border="0" /></a>
      </td>
      <?php endif; ?>
      <td><a href="./admin_log.php?uid=<?php echo $value['author'].$isdraft;?>"><?php echo $author; ?></a></td>
      <td><a href="./admin_log.php?sid=<?php echo $value['sortid'].$isdraft;?>"><?php echo $sortName; ?></a></td>
      <td class="small"><?php echo $value['date']; ?></td>
      <td class="tdcenter"><a href="comment.php?gid=<?php echo $value['gid']; ?>"><?php echo $value['comnum']; ?></a></td>
      <td class="tdcenter"><?php echo $value['views']; ?></a></td>
      </tr>
    <?php endforeach;else:?>
      <tr><td class="tdcenter" colspan="8">تېخى ئەسەر يوق</td></tr>
    <?php endif;?>
    </tbody>
    </table>
    <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
    <input name="operate" id="operate" value="" type="hidden" />
    <div class="list_footer form-inline">
    <a href="javascript:void(0);" id="select_all">ھەممىنى تاللاش</a> تاللىغاننى：
    <a href="javascript:logact('del');" class="care">ئۆچۈرۈش</a> | 
    <?php if($pid == 'draft'): ?>
    <a href="javascript:logact('pub');">يوللاش</a>
    <?php else: ?>
    <a href="javascript:logact('hide');">ئورگىنالغا ساقلاش</a> | 

    <?php if (ROLE == ROLE_ADMIN):?>
    <select name="top" id="top" onChange="changeTop(this);" style="width:120px;" class="form-control">
        <option value="" selected="selected">چوققىلاش تەڭشەش...</option>
        <option value="top">باشبەتكە چوققىلاش</option>
        <option value="sortop">تۈرگە چوققىلاش</option>
        <option value="notop">چوققىسىزلاش</option>
    </select>
    <?php endif;?>

    <select name="sort" id="sort" onChange="changeSort(this);" style="width:120px;" class="form-control">
    <option value="" selected="selected">تۈرگە يۆتكەش...</option>

    <?php 
    foreach($sorts as $key=>$value):
    if ($value['pid'] != 0) {
        continue;
    }
    ?>
    <option value="<?php echo $value['sid']; ?>"><?php echo $value['sortname']; ?></option>
    <?php
        $children = $value['children'];
        foreach ($children as $key):
        $value = $sorts[$key];
    ?>
    <option value="<?php echo $value['sid']; ?>">&nbsp; &nbsp; &nbsp; <?php echo $value['sortname']; ?></option>
    <?php
    endforeach;
    endforeach;
    ?>
    <option value="-1">تۈر يوق</option>
    </select>

    <?php if (ROLE == ROLE_ADMIN && count($user_cache) > 1):?>
    <select name="author" id="author" onChange="changeAuthor(this);" style="width:120px;" class="form-control">
    <option value="" selected="selected">ئاپتور ئۆزگەرتىش...</option>
    <?php foreach($user_cache as $key => $val):
    $val['name'] = $val['name'];
    ?>
    <option value="<?php echo $key; ?>"><?php echo $val['name']; ?></option>
    <?php endforeach;?>
    </select>
    <?php endif;?>

    <?php endif;?>
    </div>
</form>
<div class="page"><?php echo $pageurl; ?> (بار<?php echo $logNum; ?> پارچە<?php echo $pid == 'draft' ? 'ئورگىنال' : 'يازما'; ?>)</div>
</div>
<script>
$(document).ready(function(){
    $("#f_t_tag").click(function(){$("#f_tag").toggle();$("#f_sort").hide();$("#f_user").hide();});
    selectAllToggle();
});
setTimeout(hideActived,2600);
function logact(act){
    if (getChecked('ids') == false) {
        alert('يازما تاللاڭ');
        return;}
    if(act == 'del' && !confirm('راستىنلا ئۆچۈرەمسىز؟？')){return;}
    $("#operate").val(act);
    $("#form_log").submit();
}
function changeSort(obj) {
    if (getChecked('ids') == false) {
        alert('يازما تاللاڭ');
        return;}
    if($('#sort').val() == '')return;
    $("#operate").val('move');
    $("#form_log").submit();
}
function changeAuthor(obj) {
    if (getChecked('ids') == false) {
        alert('يازما تاللاڭ');
        return;}
    if($('#author').val() == '')return;
    $("#operate").val('change_author');
    $("#form_log").submit();
}
function changeTop(obj) {
    if (getChecked('ids') == false) {
        alert('يازما تاللاڭ');
        return;}
    if($('#top').val() == '')return;
    $("#operate").val(obj.value);
    $("#form_log").submit();
}
function selectSort(obj) {
    window.open("./admin_log.php?sid=" + obj.value + "<?php echo $isdraft?>", "_self");
}
function selectUser(obj) {
    window.open("./admin_log.php?uid=" + obj.value + "<?php echo $isdraft?>", "_self");
}
<?php if ($isdraft) :?>
$("#menu_draft").addClass('active');
<?php else:?>
$("#menu_log").addClass('active');
<?php endif;?>
</script>
