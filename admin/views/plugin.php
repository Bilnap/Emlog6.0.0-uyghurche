﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="containertitle"><b>قىستۇرما باشقۇرۇش</b><div id="msg"></div>
<?php if(isset($_GET['activate_install'])):?><span class="alert alert-success">قىستۇرما يوللاندى، ئاكتىپلاڭ</span><?php endif;?>
<?php if(isset($_GET['active'])):?><span class="alert alert-success">ئاكتىپلاندى</span><?php endif;?>
<?php if(isset($_GET['activate_del'])):?><span class="alert alert-success">ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_error'])):?><span class="alert alert-danger">ئاكتىپلانمىدى</span><?php endif;?>
<?php if(isset($_GET['inactive'])):?><span class="alert alert-success">چەكلەندى</span><?php endif;?>
<?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">ئۆچۈرەلمىدى ، قىستۇرما ھوقوقىنى تەكشۈرۈپ بېقىڭ</span><?php endif;?>
<?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">چىقىرالمىدى ، قىستۇرما مۇندەرىجىسى(content/plugins)غا مەشغۇلات قىلالمىدى</span><?php endif;?>
<?php if(isset($_GET['error_c'])):?><span class="alert alert-danger">بوشلۇق zip ھۆججەت فورماتىنى قوللىمىدى ، قولدا قاچىلاڭ</span><?php endif;?>
<?php if(isset($_GET['error_d'])):?><span class="alert alert-danger">بىر zip قىستۇرما ھۆججىتىنى تاللاپ بېرىڭ</span><?php endif;?>
<?php if(isset($_GET['error_e'])):?><span class="alert alert-danger">قاچىلانمىدى ، قىستۇرما ھۆججىتى ئۆلچەملىك ئەمەس</span><?php endif;?>
<?php if(isset($_GET['error_f'])):?><span class="alert alert-danger">پەقەت zip فورماتىدىكى پىرىس ھۆججىتىنى قوللايدۇ</span><?php endif;?>
</div>
<div class=line></div>
  <table class="table table-striped table-bordered table-hover dataTable no-footer">
  <thead>
      <tr>
        <th width="200"></th>
        <th width="80" class="tdcenter"><b>ئېچىش/چەكلەش</b></th>
        <th width="60" class="tdcenter"><b>نەشىرى</b></th>
        <th width="450" class="tdcenter"><b>چۈشەندۈرلىشى</b></th>
        <th width="60" class="tdcenter"></th>
      </tr>
  </thead>
  <tbody>
    <?php 
    if($plugins):
    $i = 0;
    foreach($plugins as $key=>$val):
        $plug_state = 'inactive';
        $plug_action = 'active';
        $plug_state_des = 'بېسىپ ئاكتىپلاڭ';
        if (in_array($key, $active_plugins))
        {
            $plug_state = 'active';
            $plug_action = 'inactive';
            $plug_state_des = 'بېسىپ چەكلەڭ';
        }
        $i++;
        if (TRUE === $val['Setting']) {
            $val['Name'] = "<a href=\"./plugin.php?plugin={$val['Plugin']}\" title=\"بېسىپ تەڭشەڭ\">{$val['Name']} <img src=\"./views/images/set.png\" border=\"0\" /></a>";
        }
    ?>	
      <tr>
        <td class="tdcenter"><?php echo $val['Name']; ?></td>
        <td class="tdcenter" id="plugin_<?php echo $i;?>">
        <a href="./plugin.php?action=<?php echo $plug_action;?>&plugin=<?php echo $key;?>&token=<?php echo LoginAuth::genToken(); ?>"><img src="./views/images/plugin_<?php echo $plug_state; ?>.gif" title="<?php echo $plug_state_des; ?>" align="absmiddle" border="0"></a>
        </td>
        <td class="tdcenter"><?php echo $val['Version']; ?></td>
        <td>
        <?php echo $val['Description']; ?>
        <?php if ($val['Url'] != ''):?><a href="<?php echo $val['Url'];?>" target="_blank">تېخىمۇ كۆپ ئۇچۇر&raquo;</a><?php endif;?>
        <div style="margin-top:5px;">
        <?php if ($val['ForEmlog'] != ''):?>ئىشلەتكۈچىemlog：<?php echo $val['ForEmlog'];?>&nbsp | &nbsp<?php endif;?>
        <?php if ($val['Author'] != ''):?>
        ئاپتور：<?php if ($val['AuthorUrl'] != ''):?>
            <a href="<?php echo $val['AuthorUrl'];?>" target="_blank"><?php echo $val['Author'];?></a>
            <?php else:?>
            <?php echo $val['Author'];?>
            <?php endif;?>
        <?php endif;?>
        </div>
        </td>
        <td class="tdcenter">
            <a href="javascript: em_confirm('<?php echo $key; ?>', 'plu', '<?php echo LoginAuth::genToken(); ?>');" class="care">ئۆچۈرۈش</a>
        </td>
      </tr>
    <?php endforeach;else: ?>
      <tr>
        <td class="tdcenter" colspan="5">تېخى قىستۇرما قاچىلانمىغان</td>
      </tr>
    <?php endif;?>
    </tbody>
  </table>
<div><a href="javascript:displayToggle('plugin_new', 2);" class="btn btn-success">قىستۇرما قاچىلاش +</a></div>
<form action="./plugin.php?action=upload_zip" method="post" enctype="multipart/form-data" >
    <div id="plugin_new" class="form-group" style="margin:50px 0px;">
        <li>zip فورماتىدىكى قىستۇرما ھۆججىتىنى تاللاڭ</li>
        <li>
            <input name="pluzip" type="file" />
        </li>
        <li>
            <input type="submit" value="چىقىرىش" class="btn btn-primary" />
            <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
        </li>
        <li style="margin:10px 0px;">تېخىمۇ كۆپ قىستۇرما：<a href="store.php">ئەپ بازىرى&raquo;</a></li>
    </div>
</form>
<script>
$("#plugin_new").css('display', $.cookie('em_plugin_new') ? $.cookie('em_plugin_new') : 'none');
$("#adm_plugin_list tbody tr:odd").addClass("tralt_b");
$("#adm_plugin_list tbody tr")
    .mouseover(function(){$(this).addClass("trover")})
    .mouseout(function(){$(this).removeClass("trover")})
setTimeout(hideActived,2600);
$("#menu_category_sys").addClass('active');
$("#menu_sys").addClass('in');
$("#menu_plug").addClass('active');
</script>
