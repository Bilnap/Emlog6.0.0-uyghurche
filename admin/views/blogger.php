﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');}?>
<div class="panel-heading">
<?php if (ROLE == ROLE_ADMIN):?>
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="./configure.php">ئاساسى تەڭشەك</a></li>
    <li role="presentation"><a href="./seo.php">SEOتەڭشەك</a></li>
    <li role="presentation" class="active"><a href="./blogger.php">ئەزا تەڭشەش</a></li>
    <?php if(isset($_GET['active_edit'])):?><span class="alert alert-success">ئۆزگەرتىلدى</span><?php endif;?>
    <?php if(isset($_GET['active_del'])):?><span class="alert alert-success">ئۆچۈرۈلدى</span><?php endif;?>
    <?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">ئىسىم ئۇزۇن بولۇپ قالمىسۇن</span><?php endif;?>
    <?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">ئېلخەت فورماتى خاتا</span><?php endif;?>
    <?php if(isset($_GET['error_c'])):?><span class="alert alert-danger">پارول 6 خانىدىن كەم بولمىسۇن</span><?php endif;?>
    <?php if(isset($_GET['error_d'])):?><span class="alert alert-danger">ئىككى قېتىملىق پارول بىردەك ئەمەس</span><?php endif;?>
    <?php if(isset($_GET['error_e'])):?><span class="alert alert-danger">بۇ كىرىش ئىسمى مەۋجۈت</span><?php endif;?>
    <?php if(isset($_GET['error_f'])):?><span class="alert alert-danger">بۇ نام مەۋجۈت</span><?php endif;?>
</ul>
<?php else:?>
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active"><a href="./blogger.php">ئەزا تەڭشەش</a></li>
</ul>
<?php endif;?>
</div>
<form action="blogger.php?action=update" method="post" name="blooger" id="blooger" enctype="multipart/form-data">
<div class="form-group" style="margin-left:30px;">
    <li><?php echo $icon; ?><input type="hidden" name="photo" value="<?php echo $photo; ?>"/></li>
    <li>
    <label>باش سۈرەت(JPG ۋە PNG فورماتىدىكى رەسىملەرنى قوللايدۇ)</label>
    <input name="photo" type="file" />
    </li>
    <li><label>ئىسىم</label><input maxlength="50" style="width:200px;" class="form-control" value="<?php echo $nickname; ?>" name="name" /> </li>
    <li><label>ئېلخەت</label><input name="email" class="form-control" value="<?php echo $email; ?>" style="width:200px;" maxlength="200" /></li>
    <li><label>تونۇشتۇرلىشى</label><textarea name="description" class="form-control" style="width:300px; height:65px;" type="text" maxlength="500"><?php echo $description; ?></textarea></li>
    <li><label>كىرىش ئىسمى</label><input maxlength="200" style="width:200px;" class="form-control" value="<?php echo $username; ?>" name="username" /></li>
    <li><label>يېڭى پارول (6 خانىدىن چوڭ بولسۇن)</label><input type="password" maxlength="200" class="form-control" style="width:200px;" value="" name="newpass" /></li>
    <li><label>قايتىلانما پارول</label><input type="password" maxlength="200" class="form-control" style="width:200px;" value="" name="repeatpass" /></li>
    <li>
        <input name="token" id="token" value="<?php echo LoginAuth::genToken(); ?>" type="hidden" />
        <input type="submit" value="ئارخىپ ساقلاش" class="btn btn-primary" />
    </li>
</div>
</form>
<script>
$("#chpwd").css('display', $.cookie('em_chpwd') ? $.cookie('em_chpwd') : 'none');
setTimeout(hideActived, 2600);
$("#menu_category_sys").addClass('active');
$("#menu_sys").addClass('in');
$("#menu_setting").addClass('active');
</script>
