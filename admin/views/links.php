﻿<script type="text/javascript"src="views/js/Qarluq.UIME.js"></script>
<?php if(!defined('EMLOG_ROOT')) {exit('error!');} ?>
<div class="containertitle"><b>ئۇلىنىش باشقۇرۇش</b>
<?php if(isset($_GET['active_taxis'])):?><span class="alert alert-success">تەرتىپ يېڭىلاندى</span><?php endif;?>
<?php if(isset($_GET['active_del'])):?><span class="alert alert-success">ئۆچۈرۈلدى</span><?php endif;?>
<?php if(isset($_GET['active_edit'])):?><span class="alert alert-success">ئۆزگەرتىلدى</span><?php endif;?>
<?php if(isset($_GET['active_add'])):?><span class="alert alert-success">قوشۇلدى</span><?php endif;?>
<?php if(isset($_GET['error_a'])):?><span class="alert alert-danger">بىكەت ئسىمى ۋە ئادرىسى قۇرۇق قالمىسۇن</span><?php endif;?>
<?php if(isset($_GET['error_b'])):?><span class="alert alert-danger">تەرتىپ تىزغۇدەك ئۇلىنىش يوق</span><?php endif;?>
</div>
<div class=line></div>
<form action="link.php?action=link_taxis" method="post">
  <table class="table table-striped table-bordered table-hover dataTable no-footer">
    <thead>
      <tr>
        <th width="50"><b>تەرتىپ</b></th>
        <th width="230"><b>ئۇلىنىش</b></th>
        <th width="80" class="tdcenter"><b>ھالەت</b></th>
        <th width="80" class="tdcenter"><b>كۆرۈش</b></th>
        <th width="400"><b>چۈشەندۈرىلىش</b></th>
        <th width="100"></th>
      </tr>
    </thead>
    <tbody>
    <?php 
    if($links):
    foreach($links as $key=>$value):
    doAction('adm_link_display');
    ?>  
      <tr>
        <td><input class="form-control em-small" name="link[<?php echo $value['id']; ?>]" value="<?php echo $value['taxis']; ?>" maxlength="4" /></td>
        <td><a href="link.php?action=mod_link&amp;linkid=<?php echo $value['id']; ?>" title="ئۇلىنىش ئۆزگەرتىش"><?php echo $value['sitename']; ?></a></td>
        <td class="tdcenter">
        <?php if ($value['hide'] == 'n'): ?>
        <a href="link.php?action=hide&amp;linkid=<?php echo $value['id']; ?>" title="چىكىپ يۇشۇرۇڭ">ئاشكارە</a>
        <?php else: ?>
        <a href="link.php?action=show&amp;linkid=<?php echo $value['id']; ?>" title="چىكىپ كۆرسىتىڭ" style="color:red;">يۇشۇرۇن</a>
        <?php endif;?>
        </td>
        <td class="tdcenter">
        <a href="<?php echo $value['siteurl']; ?>" target="_blank" title="ئۇلىنىش كۆرۈش">
        <img src="./views/images/vlog.gif" align="absbottom" border="0" /></a>
        </td>
        <td><?php echo $value['description']; ?></td>
        <td>
        <a href="link.php?action=mod_link&amp;linkid=<?php echo $value['id']; ?>">تەھرىرلەش</a>
        <a href="javascript: em_confirm(<?php echo $value['id']; ?>, 'link', '<?php echo LoginAuth::genToken(); ?>');" class="care">ئۆچۈرۈش</a>
        </td>
      </tr>
    <?php endforeach;else:?>
      <tr><td class="tdcenter" colspan="6">تېخى ئۇلىنىش قوشۇلمىغان</td></tr>
    <?php endif;?>
    </tbody>
  </table>
  <div class="list_footer">
      <input type="submit" value="تەرتىپ ئۆزگەرتىش" class="btn btn-primary" /> 
      <a href="javascript:displayToggle('link_new', 2);" class="btn btn-success">ئۇلىنىش قوشۇش +</a>
  </div>
</form>
<form action="link.php?action=addlink" method="post" name="link" id="link" class="form-inline">
<div id="link_new" class="form-group">
    <li>
        <input maxlength="4" style="width:30px;" class="form-control" name="taxis" />
        <label>تەرتىپ</label>
    </li>
    <li>
        <input maxlength="200" style="width:232px;" class="form-control" name="sitename" />
        <label>ئىسمى<span class="required">*</sapn></label>
    </li>
    <li>
        <input maxlength="200" style="width:232px;" class="form-control" name="siteurl" />
        <label>ئادرىسى<span class="required">*</sapn></label>
    </li>
    <li>چۈشەندۈرىلىش</li>
    <li><textarea name="description" type="text" class="form-control" style="width:230px;height:60px;overflow:auto;"></textarea></li>
    <li><input type="submit" class="btn btn-primary" name="" value="ئۇلىنىش قوشۇش"  /></li>
</div>
</form>
<script>
$("#link_new").css('display', $.cookie('em_link_new') ? $.cookie('em_link_new') : 'none');
$(document).ready(function(){
    $("#adm_link_list tbody tr:odd").addClass("tralt_b");
    $("#adm_link_list tbody tr")
        .mouseover(function(){$(this).addClass("trover")})
        .mouseout(function(){$(this).removeClass("trover")})
});
setTimeout(hideActived,2600);
$("#menu_link").addClass('active');
</script>
